<?php

$uri    = $_SERVER['REQUEST_URI'];
$uri    = $uri == '/' ? '/index' : $uri;
$uri    = substr($uri, 1, strlen($uri));
$layout = file_get_contents('layout.phtml');

$page_path = realpath('pages/' . $uri . '.phtml');
if ( file_exists($page_path) ) {
    $page = file_get_contents($page_path);
} else {
    $page = file_get_contents('pages/404.phtml');
}
$layout = str_replace('{{content}}', $page, $layout);
file_put_contents(1, $layout);
include '1';