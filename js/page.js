function onResize() {
    if ($(window).width() < 1024) {
        if (!$('body').hasClass('mobile')) {
            $('body').addClass('mobile');
            $('.sponsor-block, .info-bl', 'main .top-block').remove().insertBefore('main .top-block .toggle-info-btn');
        }
    } else {
        if ($('body').hasClass('mobile')) {
            $('body').removeClass('mobile');
            $('.sponsor-block, .info-bl', 'main .top-block').remove().prependTo('main .top-block');
        }
    }
}

$(function () {
    onResize();
    $(window).resize(onResize);

    //показать попап авторизации
    $('.btn-signup').click(function () {
        $('.login-popup').addClass('showed');
        return false;
    });

    //показать/скрыть блок информации в хедере для мобильной версии
    $('.toggle-info-btn').click(function () {
        $(this).toggleClass('expanded');
        $('.sponsor-block, .info-bl', '.top-block').slideToggle('fast');
        return false;
    });

    $('body').on('click', '.tabs-selector .tab', function () {
        var $tabs_selector = $('.tabs-selector'),
            tab = parseInt($(this).data('tab')),
            $head_tab1 = $('[data-tab=1]', $tabs_selector),
            $head_tab2 = $('[data-tab=2]', $tabs_selector),
            $head_tab3 = $('[data-tab=3]', $tabs_selector);

        //переключаем по кругу табы
        switch (tab) {
            case 1:
                $head_tab2.show();
                $head_tab3.show().remove().prependTo($tabs_selector);
                $head_tab1.hide().remove().appendTo($tabs_selector);
                break
            case 2:
                $head_tab3.show();
                $head_tab1.show().remove().prependTo($tabs_selector);
                $head_tab2.hide().remove().appendTo($tabs_selector);
                break
            case 3:
                $head_tab1.show();
                $head_tab2.show().remove().prependTo($tabs_selector);
                $head_tab3.hide().remove().appendTo($tabs_selector);
                break
        }
        $('.tab-block').hide();
        $('.tab-block[data-tab=' + tab + ']').show();
    });

    $('.mobile-tours-block .tour-block').click(function () {
        var $mobile_tours_block = $(this).parents('.mobile-tours-block');
        $mobile_tours_block.toggleClass('expanded');
        $mobile_tours_block.find('.tours-list').fadeToggle('fast');
    });

    //открыть форму авторизации в табе
    $('.open-signup-form').click(function () {
        $('.tab-block .signin-form').fadeOut('fast', function () {
            $('.tab-block .signup-form').fadeIn('fast');
        });
        return false;
    });

    //открыть форму регистрации в табе
    $('.open-signin-form').click(function () {
        $('.tab-block .signup-form').fadeOut('fast', function () {
            $('.tab-block .signin-form').fadeIn('fast');
        });
        return false;
    });

    //переключить вид для партий
    $('.switch-mode-block .switcher').click(function () {
        var $parties_table = $('.parties-table'),
            $parties_boards = $('.parties-boards');

        if ($(this).hasClass('boards')) {
            $(this).data('mode', 'table').removeClass('boards').addClass('table');
            $parties_table.show();
            $parties_boards.hide();
        } else {
            $(this).data('mode', 'boards').removeClass('table').addClass('boards');
            $parties_table.hide();
            $parties_boards.show();
        }
    });
});