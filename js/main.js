function closePopups($filter) {
    var $popups = $filter ? $('.popup:not("' + $filter.selector + '")') : $('.popup');
    $popups.removeClass('showed');
    $('.icons-block a').removeClass('selected');
}

var selectors_proto = function () {
    var self = this;
    this.bind = function () {
        $('.selector').click(function () {
            $(this).find('.list-extended').fadeIn('fast');
            return false;
        });

        $('.selector .list-extended .hide-block').click(function () {
            $(this).parents('.selector').find('.list-extended').fadeOut('fast');
            return false;
        });

        $('.selector .list-extended li').click(function () {
            var $selector = $(this).parents('.selector'),
                $tour = $(this).find('.caption');
            $('.selected-variant', $selector).html($tour.html());
            $('.list-extended', $selector).fadeOut('fast');
            return false;
        });
    };

    this.init = function () {
        self.bind();
    };
    self.init();
};

function hideSidebarMenu() {
    var $desktop_sidebar_menu = $('.desktop-sidebar-menu');
    $desktop_sidebar_menu.removeClass('opened');
    setTimeout(function () {
        $desktop_sidebar_menu.hide();
    }, 400);

    $('body').removeClass('menu-opened');
}

$(function () {
    var $desktop_sidebar_menu = $('.desktop-sidebar-menu');
    var selectors = new selectors_proto();
    if ($(window).width() > 1023) {
        $('.menu-sidebar .nav-wr').scrollbar();
    }

    //открыть меню
    $('header .open-menu').click(function () {
        if (document.messenger) {
            messenger.close();
        }
        $('body').addClass('menu-opened');
        $desktop_sidebar_menu.show(0, function () {
            $desktop_sidebar_menu.addClass('opened')
        });
        var parties_height = $('.current-parties-wr').height() - 100,
            document_height = $(document).height() - 100;

        $('.current-parties').height(parties_height > document_height ? parties_height : document_height);
        $('.parties-popup').fadeOut('fast');
        return false;
    });

    //скрыть меню
    $($desktop_sidebar_menu).click(function () {
        hideSidebarMenu();
    });

    var $auth_popup = $('header .auth-popup');
    //показать попап авторизации
    $('.open-user').click(function () {
        hideSidebarMenu();
        if ($auth_popup.hasClass('showed')) {
            closePopups();
        } else {
            closePopups($auth_popup);
            $auth_popup.addClass('showed');
            $(this).addClass('selected');
        }
        return false;
    });

    //закрыть попап авторизации
    $('.head', $auth_popup).click(function () {
        closePopups();
        $('header .open-user').removeClass('selected');
        return false;
    });

    $('.to-login-form-btn', $auth_popup).click(function () {
        $('.btns-wr', $auth_popup).hide();
        $('form', $auth_popup).show();
        return false;
    });
    $('.back-link', $auth_popup).click(function () {
        $('.btns-wr', $auth_popup).show();
        $('form', $auth_popup).hide();
        return false;
    });
});