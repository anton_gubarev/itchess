var party_page_proto = function () {
    var self = this;

    this.messenger = null;

    this.onResize = function () {
        var $body = $('body');
        if ($(window).width() < 1024) {
            if (!$body.hasClass('mobile')) {
                $body.addClass('mobile');
                $('.player-btns-wr').remove().insertBefore('.sponsor-block');
                $('.stream-block').hide();
                $('.nonstream-block').show();
            }
        } else {
            if ($body.hasClass('mobile')) {
                $body.removeClass('mobile');
                $('.player-btns-wr').remove().insertAfter('.right-block .nonstream-block .btns-wr');
            }
        }
    };
    //установить шанс от -5 до 5
    this.changeChance = function (value) {
        value += 5;
        $('.chance-line .chance').css({height: "calc(5% + (100%/11)*" + value + ")"});
    };

    this.bind = function () {
        $(window).resize(self.onResize);

        //переключение темы доски
        $('.settings-block .variants li').click(function () {
            $('.variants li').removeClass('selected');
            $(this).addClass('selected');
        });

        //открыть/скрыть настройки
        $('header .open-settings').click(function () {
            var $settings_block = $('.settings-block');
            if ($settings_block.hasClass('showed')) {
                closePopups();
            } else {
                closePopups($settings_block);
                $settings_block.height($(document).height() - 110)
                    .addClass('showed');
                $('header .open-settings').addClass('selected');
            }
            return false;
        });

        //закрыть настройки
        $('.close-settings').click(function () {
            closePopups();
            return false;
        });

        $('.table').scrollbar();

        //показать стрим
        $('.btn-show-stream').click(function () {
            $('.stream-popup').fadeIn('fast');
            return false;
        });

        $('.stream-popup .close').click(function () {
            $('.stream-popup').fadeOut('fast');
            return false;
        });
        $('.stream-popup .min').click(function () {
            $('.stream-popup').fadeOut('fast');
            return false;
        });

        //кнопка сворачивания/разворачивания блока с анализом партии
        $('.analysis .slide-btn').click(function () {
            $(this).toggleClass('slide-up slide-down');
            $('.analysis-block').slideToggle('fast');
            return false;
        });

        //кнопки плеера
        $('.player-btns-wr .b').click(function () {
            //тут какие-то действия
            return false;
        });

        //скролл на мобильной версии списка матчей
        $('.match-block-extended .matches').scrollbar();

        //развернуть блок с матчами
        $('.mobile-top-block .match-block').click(function () {
            $('.mobile-top-block .match-block-extended').fadeIn('fast');
            return false;
        });
        //свернуть блок с матчами
        $('.mobile-top-block .match-block-extended .current-match').click(function () {
            $('.mobile-top-block .match-block-extended').fadeOut('fast');
            return false;
        });

        //открыть попап с партиями
        $('.all-parties-btn').click(function () {
            closePopups('.parties-popup');
            $('.parties-popup')
                .addClass('showed');
            return false;
        });

        //закрыть настройки
        $('.close-parties').click(function () {
            closePopups();
            return false;
        });

        //закрыть вкладку
        $('.top-block nav a .close').click(function () {
            $(this).parents('a').hide();
            return false;
        });

        $('.game-block .step').click(function () {
            $('.game-block .step').removeClass('selected');
            $(this).toggleClass('selected');
        });

        //выбор матча из списка
        $('body').on('click', '.parties-popup .parties-table li', function () {
            var $table = $('.parties-popup .parties-table');
            if ($(this).hasClass('selected')) {
                var order = parseInt($(this).data('order')),
                    $prev_li = $('.parties-popup .parties-table li:last'),
                    founded = false;

                $('.parties-popup .parties-table li:not(.selected)').each(function () {
                    if (!founded && parseInt($(this).data('order')) > order) {
                        $prev_li = $(this);
                        founded = true;
                    }
                });
                $(this).remove()
                    .removeClass('selected')
                    .addClass('unselected')
                    .insertBefore($prev_li);
            } else {
                $(this).addClass('selected')
                    .removeClass('unselected')
                    .remove()
                    .prependTo($table);
            }
        });

        //переключение кнопки плей/пауза
        $('.player-btns-wr .play').click(function () {
            $(this).toggleClass('play pause');
        });
    };

    this.init = function () {
        self.bind();
        self.onResize();
        self.messenger = new messenger_proto();

        $('.stream-popup').draggable({
            containment: ".body-content",
            handle: '.head'
        });

        //это для теста!!!
        setInterval(function () {
            var value = Math.round(-5 + Math.random() * 10);
            self.changeChance(value);
        }, 2000);
    };

    self.init();
};

var messenger_proto = function () {
    var self = this;
    this.$messenger = $('.messenger');

    this.close = function () {
        self.$messenger.removeClass('showed');
    };

    this.bind = function () {
        //показать мессенджер
        $('header .open-messenger').click(function () {
            if ($(window).width() < 1024) {
                location = '/messenger';
            } else {
                closePopups(self.$messenger);
                self.$messenger.addClass('showed');
                return false;
            }
        });

        //скрыть мессенджер
        $('.icon-close', self.$messenger).click(function () {
            self.close();
            return false;
        });
    };

    this.init = function () {
        self.bind();
        $('.messages', self.$messenger).scrollbar();
        self.$messenger.draggable({
            containment: "body",
            handle: '.title'
        });
    };


    self.init();
};

$(function () {
    party_page = new party_page_proto();
});